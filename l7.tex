\documentclass[14pt,tikz, border=5mm]{article}
\usepackage{amsmath}% http://ctan.org/pkg/amsmath
\usepackage{listings}% http://ctan.org/pkg/listings
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage{amssymb}
\usepackage{fancyhdr}
\usepackage{algorithm}
\usepackage{algorithmicx}
\usepackage{algpseudocode}
\usepackage{url}
\usepackage{tikz}
\usepackage{tkz-graph}

\usetikzlibrary{positioning}
\usetikzlibrary{shapes.geometric}
\tikzset{main node/.style={circle,fill=blue!20,draw,minimum size=1cm,inner sep=0pt}, }

\tikzstyle{VertexStyle} = [shape = ellipse, minimum width = 6ex, draw]
\tikzstyle{EdgeStyle}   = [->,>=stealth']

\title{MAC-5711\\Análise de Algoritmos\\Lista 7}
\author{Dylan Jefferson M. G. Guedes\\NUSP: 10555804}

%fncyhrd
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Dylan Guedes - 10555804}
\rhead{MAC-5711 - Lista 7}

\begin{document}
\maketitle

\begin{enumerate}
    \setcounter{enumi}{1}

    \item ~
        O Algoritmo de Dijkstra atua de forma gulosa, fazendo relaxamento de um
        vértice $u$ sempre que passar por uma aresta $k$ no caminho
        $s \leadsto u$ tenha custo menor que o menor caminho $s \leadsto u$ já
        visto. Ou seja, Dijkstra passa a produzir resultados errados porque
        passar por uma aresta de custo estritamente negativo infinitas vezes
        sempre resultará num caminho menor que o já visto, de modo que a
        corretude do algoritmo não será mantida.

        No grafo abaixo, por exemplo, caso quiséssemos utilizar Dijkstra a
        partir do vértice $s$, o algoritmo poderia produzir resultados errados
        pois passar outra vez pela aresta $(b, e) = -5$ reduzirá o custo
        total do menor caminho visto até então.

        \tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
        \tikzset{edge/.style = {->,> = latex'}}

        \begin{tikzpicture}[shorten >=1pt, auto, node distance=3cm, ultra thick,
            node_style/.style={circle,draw=blue,fill=blue!20!,font=\sffamily\Large\bfseries},
                  edge_style/.style={->,> = latex',draw=black, ultra thick}]
            \node[node_style] (a) at (-4, 0) {s};
            \node[node_style] (b) at (-2, 2) {b};
            \node[node_style] (c) at (-2, -2) {c};
            \node[node_style] (d) at (2, 2) {d};
            \node[node_style] (e) at (2, -2) {e};
            \node[node_style] (f) at (4, 0) {t};

            \draw[edge_style] (a) edge node{1} (b);
            \draw[edge_style] (a) edge node{1} (c);
            \draw[edge_style] (b) edge node{1} (d);
            \draw[edge_style] (b) edge node{-5} (e);
            \draw[edge_style] (e) edge node{3} (f);
            \draw[edge_style] (c) edge node{3} (b);
            \draw[edge_style] (e) edge node{3} (c);
            \draw[edge_style] (d) edge node{4} (f);

        \end{tikzpicture}

    \item ~
        Uma maneira de resolver o problema sem alterar substancialmente o código
        do algoritmo de Dijkstra se baseia em:
        \begin{itemize}
            \item Adicionar dois vértices (os chamarei de $s'$ e $t'$) ao grafo
                $G$;
            \item Ligar $s'$ a todo vértice $k \in S$ utilizando uma aresta de
                peso zero;
            \item Ligar todo vértice $k \in T$ ao vértice $t'$ utilizando uma
                aresta de peso zero;
            \item Calcular o menor caminho de $s'$ a $t'$.
        \end{itemize}

        Essa abordagem produz resultados corretos pois, como o peso das arestas
        que ligam $s'$ aos vértices pertencentes a $S$ é 0, não faz diferença que
        o menor caminho comece em $s'$ ou em qualquer nó $u \in S$, pois caso
        fosse calculado o menor caminho a partir de um vértice $u$ específico, adicionar
        a esse menor caminho uma aresta de peso 0 faz com que o tamanho total
        seja o mesmo. Da mesma forma, calcular o menor caminho até um vértice
        que pertença a $T$ é o mesmo que calcular o menor caminho até $t'$,
        afinal dado o menor caminho até $T$, adicionar uma outra aresta de peso
        zero que ligue $T$ a $t'$ mantém o tamanho total do menor caminho. O
        algoritmo abaixo resolve o problema utilizando essas observações:

        \pagebreak

        \begin{algorithm}[H]
            \caption{Menor caminho do conjunto $S$ até o conjunto $T$.}
            \label{alg:dijkstra}
            \begin{algorithmic}[1]
            \Function{distancia-s-t}{$S, T, c, adj$}
                \State $s' \gets $\Call{novo-vertice}{}
                \State $t' \gets $\Call{novo-vertice}{}

                \For{$u \in S$}
                    \State $adj[s'] \gets adj[s'] \cup (s', u)$\Comment{Adiciona
                    a aresta $(s', u)$}
                    \State $c(s', u) \gets 0$\Comment{Define o peso da aresta
                    $(s', u)$ como zero.}
                \EndFor

                \For{$u \in T$}
                    \State $adj[u] \gets adj[u] \cup (u, t')$\Comment{Adiciona a
                    aresta $(u, t')$}
                    \State $c(u, t') \gets 0$\Comment{Define o peso da aresta
                    $(u, t')$ como zero.}
                \EndFor

                \State $G \gets $\Call{novo-grafo}{}\Comment{Aloca um novo
                grafo}
                \State $V(G) \gets \{S\} \cup \{T\} \cup \{s'\} \cup
                \{t'\}$\Comment{Os vértices de $G$ são todos em $S, T$, mais
                $s'$ e $t'$.}

                \State \Return{\Call{dijkstra}{$G, s', t', c, adj$}}

            \EndFunction

            \Function{dijkstra}{$G, s', t', c, adj$}
                \Comment{Os parâmetros representam, respectivamente: o grafo
                utilizado, a origem do menor caminho, o destino do menor caminho 
                (que será retornado), os comprimentos das arestas e as
                adjacências.}
                \State $n \gets |V(G)|$
                \State $Q \gets \{\varnothing\}$
                \State $Q \gets V(G)$
                \For{$u \in V(G)$}
                    \State $u.d \gets \infty$
                \EndFor
                \State $s'.d \gets 0$
                \While{$Q \neq \varnothing$}
                    \State $u \gets $\Call{extract-min}{$Q$}
                    \For{$v \in adj[u]$}
                        \If{$(v \in Q) \wedge (v.d > u.d + c(u, v))$}
                            \State $v.d \gets u.d + c(u, v)$
                        \EndIf
                    \EndFor
                \EndWhile

                \State \Return{$t'.d$}
            \EndFunction
        \end{algorithmic}
        \end{algorithm}

        Quanto ao tempo de execução, mostrarei que o algoritmo acima
        apresenta a mesma complexidade que a implementação de Dijkstra presente
        no CLRS ($O(m \log n)$), onde $n$ é o número de vértices e $m$ o número
        de arestas. A função \textit{DISTANCIA-S-T} tem a complexidade ditada
        pela função \textit{DIJKSTRA}, pois todas as outras operações (adicionar
        arestas, alocar e modelar o grafo) são lineares. Assim, só precisamos
        nos preocupar com a complexidade da versão modificada do $dijkstra$ que
        conta com $m+n$ arestas.

        Inicialmente, tome o grafo $G$, que no pior caso teria $n(n-1)$
        arestas (ou seja, seria um grafo completo, mas sem multi-arestas ou
        loops).

        Com a adição de uma aresta para cada vértice pertencente a $S$ e a $T$,
        no pior caso, onde todo vértice ou está em $S$ ou em $T$, serão
        adicionadas $n = |S| + |T|$ arestas ao grafo, de modo que o grafo passe
        a ter $m+n$ arestas.

        Através dessas observações, podemos dizer que a complexidade do
        algoritmo de Dijkstra apresentado no Algoritmo~\ref{alg:dijkstra} é da
        ordem
        \begin{equation}
            O((m+n) \log n) = O(m \log n),\footnote{
                No pior caso, quando o grafo for completo, $m>n$; então
                arredondei $m+n$ para $m$.
            }
        \end{equation}
        que é a mesma complexidade apresentada na literatura.

    \setcounter{enumi}{9}

    \item ~
        Dado um grafo $G$ com somente arestas distintas e sua MST, que chamarei
        de $T$, por contradição, imagine que $G$ possui outra MST, $T'$.

        Como $T \neq T'$, existe ao menos uma aresta $e \in T$ que não está em
        $T'$. Se essa aresta liga dois vértices $A, B \in V(G)$, por $T'$ ser uma
        MST, sabemos que $T'$ tem um caminho $A \leadsto B$ que não utiliza a
        aresta $e$. Agora, adicione $e$ a $T'$, de modo que um ciclo seja criado
        (pois já existia um caminho $A \leadsto B$). Nesse ciclo com certeza
        existe uma aresta $f \in T'$ que não está em $T$ (pois $T$ não tinha
        ciclos e já tinha um caminho $A \leadsto B$).

        Por fim, remova $f$ de $T'$, de modo que $T'$ volte a não ter ciclos.
        Mas, $f$ necessariamente tem peso diferente de $e$, de modo que o peso
        total de $T'$ ou é menor ou maior a seu peso inicial. Para ambos
        os casos, ou $T'$ era a MST ou $T$, mas não os dois ao mesmo tempo - uma
        contradição. Portanto, um grafo $G$ com somente arestas de pesos
        distintos deve ter uma MST única.

    \setcounter{enumi}{12}

    \item ~
        Tome a aresta $u \in T$, onde $T$ é a MST do grafo $G$. Se $u$ for
        retirada de $T$, o grafo será dividido em dois componentes: $C_1$ e
        $C_2$. Se existir alguma aresta com peso menor que $u$ que conecte $C_1$
        a $C_2$, utilizando-a conseguiremos montar uma nova MST com peso menor
        que $T$, um absurdo. Dessa forma, $u$ tem peso mínimo no corte que
        separa $T$ nas componentes $C_1$ e $C_2$.

    \setcounter{enumi}{15}

    \item ~
        Inicialmente, tome um grafo $G$ com múltiplas MST's e, dentre essas, uma
        MST $T$ e outra $T'$, diferentes. Como $T$ e $T'$ são diferentes, existe
        ao menos $m$ arestas $k \in T$ que não estão em $T'$, e $m$ arestas
        $p \in T'$ que não estão em $T$.

        Cada aresta $k$ conecta vértices $(u, v)$, mas como $T'$ é
        uma MST, $T'$ deve ter outro caminho $u \leadsto v$ que não utiliza a
        aresta $k$. Assim, se adicionarmos $k$ em $T'$, $T'$ passará a ter um
        ciclo. Nesse novo ciclo criado existe uma aresta $y$ que não está em $T$
        (pois $T$ já tinha um caminho $u \leadsto v$), que pode ser removida de
        $T'$, fazendo com que $T'$ deixe de ter um ciclo e volte a ser uma MST
        ($y$ e $k$ devem ter o mesmo peso). Contudo, observe que a nova MST
        encontrada (ao trocar a aresta $y$ por $k$) é uma vizinha de $T'$, pois
        só trocamos uma aresta em $T'$.

        Ou seja, se inicialmente $m$ era 1, a nova $MST$ é $T$; se $m$ era 2, ao
        fazer a troca de arestas, teremos uma nova MST que só diverge de $T$ em
        uma aresta, sendo $T'$ um vizinho de $T$,
        logo existe um caminho $T \leadsto T'$; por
        fim, para $m > 2$, encontramos uma MST intermediária vizinha de $T'$ que
        tem $m-1$ arestas $k$ em relação a $T$.

        Repetindo o passo descrito anteriormente $m-1$ vezes, ao final teremos
        uma MST intermediária $U$ que, por vir de transformações de $T'$,
        naturalmente apresenta caminho até $T'$; e, como para $U$ a quantidade
        de arestas do tipo $k$ é $m = 1$, só existe uma aresta $k \in T$ que não
        está em $U$ e vice-versa, de modo que, por definição, $T \leadsto U$.
        Como $T \leadsto U$ e $U \leadsto T'$, existe um caminho
        $T \leadsto T'$, logo $\mathcal{H}$ é um grafo conectado.

\end{enumerate}

\end{document}
