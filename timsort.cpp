#include <bits/stdc++.h>

using namespace std;

#define rep(i, j, k)for(int i=j;i<k;++i)

#define MAX 10000

int *timsortlistacurta(int *A, int n)
{
        for (int i=2; i <= n; i += 1) {
                int key = A[i];
                int hi = i;
                int lo = 1;
                int mi = lo + (hi-lo)/2;
                while (hi > lo) {
                        mi = lo + (hi-lo)/2;
                        if (A[mi] > key) {
                                hi = mi;
                        } else {
                                lo = mi+1;
                        }
                }
                if (A[lo] == key) {
                        continue;
                }
                for (int j = i; j >= lo; j--) {
                        A[j] = A[j-1];
                }
                A[lo] = key;
        }
        return A;
}

int main()
{
        int n;
        int arr[MAX];
        cin  >> n;
        for (int i=1; i <= n; ++i) {
                cin >> arr[i];
        }
        int *mylist = timsortlistacurta(arr, n);
        for (int i=1; i <= n; ++i) {
                cout << mylist[i] << " ";
        }
        cout << endl;

        return 0;
}
